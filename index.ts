import { App } from "./src/app";
import { connect } from "mongoose";
import { config } from "dotenv";

async function main() {
  const app = new App();
  await app.listen();
}

config();
connect(
  process.env.MONGO_URL || "",
  {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true
  },
  (err) => {
    if (err) console.log(err);
    else {
      main();
      console.log("base de datos Online!!");
    }
  }
);
