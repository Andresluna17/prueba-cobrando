// const axios = require("axios");
if (localStorage.getItem("token")) {
  location.href = "upload.html";
}
const submitLogin = async (e) => {
  var email = document.getElementById("email").value;
  var password = document.getElementById("password").value;
  if (email != "" && password != "") {
    let data = {
      email,
      password
    };
    console.log(data);
    try {
      let response = await axios.post("/login", data);
      console.log(response.data);
      await Swal.fire({
        position: "top-end",
        icon: "success",
        title: "logeo exitoso",
        showConfirmButton: false,
        timer: 1500
      });
      localStorage.setItem("token", response.data.token);
      localStorage.setItem("user", JSON.stringify(response.data.usuario));
      location.href = "upload.html";
    } catch (error) {
      console.log(error);
    }
  }
};

const submitRegister = async (e) => {
  var email = document.getElementById("email").value;
  var password = document.getElementById("password").value;
  var nombre = document.getElementById("nombre").value;
  if (email != "" && password != "") {
    let data = {
      email,
      password,
      nombre
    };
    console.log(data);
    try {
      let response = await axios.post("/register", data);
      console.log(response.data);
      await Swal.fire({
        position: "top-end",
        icon: "success",
        title: "registro exitoso",
        showConfirmButton: false,
        timer: 1500
      });
      location.href = "login.html";
    } catch (error) {
      console.log(error);
    }
  }
};

const redireccion = (e) => {
  location.href = e;
};
