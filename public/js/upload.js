if (!localStorage.getItem("token")) {
  location.href = "index.html";
}
let user = JSON.parse(localStorage.getItem("user"));
let data = $("#user");
let html = "";
html += "<li>";
html += `${user.nombre} conectado`;
html += "</li>";

data.html(html);
const getExcel = async () => {
  let response = await axios.get("/upload", {
    headers: {
      "Authorization": localStorage.getItem("token")
    }
  });
  console.log(response.data);
  let data = $("#data");
  var html = "";
  for (i in response.data.data) {
    delete response.data.data[i]._id;
    delete response.data.data[i].user;
    delete response.data.data[i].updatedAt;
    delete response.data.data[i].createdAt;
    html += "<li>";
    html += `${Object.values(response.data.data[i])}`;
    html += "</li>";
  }
  data.html(html);
};
getExcel();

const logout = () => {
  localStorage.clear();
  location.href = "index.html";
};

const upload = async () => {
  let archivo = document.getElementById("archivo").files[0];
  let formData = new FormData();
  formData.append("archivo", archivo);
  let response = await axios.post("/upload", formData, {
    headers: {
      "Content-Type": "multipart/form-data",
      "Authorization": localStorage.getItem("token")
    }
  });
  console.log(response.data);
  Swal.fire({
    position: "top-end",
    icon: "success",
    title: response.data.message,
    showConfirmButton: false,
    timer: 1500
  });
  $("#archivo").val("");
  getExcel();
};
