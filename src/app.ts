import express, { Express } from "express";
import morgan from "morgan";
import cors from "cors";
import { AuthModule } from "./modules/auth/auth.module";
import { ExcelModule } from "./modules/excel/excel.module";
import fileUpload from "express-fileupload";
import { urlencoded } from "body-parser";
import pathJs from "path";

export class App {
  app!: Express;
  constructor(private port?: number | string) {
    this.app = express();
    this.settings();
    this.middlewares();
    this.initmodules();
  }

  private middlewares() {
    this.app.use(morgan("dev"));
    this.app.use(cors());
    this.app.use(express.json());
    this.app.use(urlencoded({ extended: false }));
    this.app.use(fileUpload());
    this.app.use(express.static(pathJs.resolve(__dirname, "../public")));
  }

  private settings() {
    this.app.set("port", this.port || process.env.PORT || 3000);
  }

  public async listen(): Promise<void> {
    await this.app.listen(this.app.get("port"));
    console.log("Server on port", this.app.get("port"));
  }

  private initmodules() {
    new AuthModule(this.app);
    new ExcelModule(this.app);
  }
}

export default new App();
