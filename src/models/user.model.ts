import mongoose from "mongoose";
import validator from "mongoose-unique-validator";

export interface IUser extends mongoose.Document {
  nombre: string;
  email: string;
  password: string;
}

export const UserSchema = new mongoose.Schema({
  nombre: {
    type: String,
    lowercase: true,
    required: [true, "El nombre es requerido"]
  },
  email: {
    type: String,
    unique: true,
    required: [true, "El correo es requerido"]
  },
  password: {
    type: String,
    required: [true, "La contraseña es necesaria"]
  }
});
UserSchema.plugin(validator, { message: "{PATH} debe de ser único" });
const User = mongoose.model<IUser>("User", UserSchema);
export default User;
