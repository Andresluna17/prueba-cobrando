import mongoose from "mongoose";

export const ExcelSchema = new mongoose.Schema(
  {},
  { versionKey: false, timestamps: true, strict: false }
);
export const Excel = mongoose.model("Excel", ExcelSchema);
