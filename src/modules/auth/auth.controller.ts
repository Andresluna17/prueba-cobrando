import { Request, Response } from "express";
import jwt, { Secret } from "jsonwebtoken";
import bcrypt from "bcrypt";
import User, { IUser } from "../../models/user.model";

export class AuthController {
  public async login(req: Request, res: Response) {
    try {
      let { email, password } = req.body;
      let user: IUser | null = await User.findOne({ email });
      if (!user) {
        return res.status(400).json({
          ok: false,
          mensaje: "Credenciales incorrectas"
        });
      }
      if (!bcrypt.compareSync(password, user.password)) {
        return res.status(400).json({
          ok: false,
          mensaje: "Credenciales incorrectas"
        });
      }
      let token = jwt.sign({ usuario: user }, process.env.SEED || "", {
        expiresIn: 14400
      }); // 4 horas

      res.status(200).json({
        ok: true,
        usuario: user,
        token: token
      });
    } catch (error) {
      console.log(error);
      return res.status(400).json({
        ok: false,
        error
      });
    }
  }
  public async register(req: Request, res: Response) {
    try {
      let { email, password, nombre } = req.body;
      let user = new User({
        nombre: nombre,
        email: email,
        password: bcrypt.hashSync(password, 10)
      });
      await user.save();
      res.status(200).json({
        ok: true,
        usuario: User
      });
    } catch (error) {
      return res.status(400).json({
        ok: false,
        error
      });
    }
  }
}
