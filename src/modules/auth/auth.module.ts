import { Express } from "express";
import { AuthController } from "./auth.controller";

export class AuthModule {
  private routeController: AuthController;

  constructor(app: Express) {
    this.routeController = new AuthController();
    app.route("/login").post(this.routeController.login);
    app.route("/register").post(this.routeController.register);
  }
}
