import { Request, Response } from "express";
import { Excel } from "../../models/excel.model";
import pathJs from "path";
import excelToJson from "convert-excel-to-json";

export class ExcelController {
  public async upload(req: Request, res: Response) {
    let { archivo } = req.files;
    let path = `../../../../uploads/${archivo.name}`;
    await archivo.mv(pathJs.resolve(__dirname + path), (error) => {
      if (error) {
        res.json({ ok: false, error });
      } else {
        let result = excelToJson({
          sourceFile: pathJs.resolve(__dirname + path),
          header: {
            rows: 1
          },
          columnToKey: {
            "*": "{{columnHeader}}"
          }
        });
        result.Hoja1.map((data) => {
          let excel = new Excel(data);
          excel.save();
        });
        res.json({ ok: true, message: "archivo guardado en la base de datos" });
      }
    });
  }

  public async getExcels(req: Request, res: Response) {
    let excels = await Excel.find().exec();
    res.json({ ok: true, data: excels });
  }
}
