import { Express } from "express";
import { ExcelController } from "./excel.controller";
import { verificaToken } from "./../../middlewares/autentication";

export class ExcelModule {
  private routeController: ExcelController;

  constructor(app: Express) {
    this.routeController = new ExcelController();
    app
      .route("/upload")
      .post(verificaToken, this.routeController.upload)
      .get(verificaToken, this.routeController.getExcels);
  }
}
